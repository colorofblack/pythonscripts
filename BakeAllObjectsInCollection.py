import bpy
# bakes all Objects in collection bake.
# Important: Create Empty Image named Bake Texture 32bit float. After Baking composite it with a denoiser to apply color management to it
col = bpy.data.collections.get("bake")

#image_name = '00_HKBake'
#img = bpy.data.images.new(image_name,4096,4096)
img = bpy.data.images['BakeTexture']

for object in col.objects: 
    print(object.name)
    bpy.context.view_layer.objects.active = object
    bpy.data.objects[object.name].select_set(True)
    for mat in object.data.materials:

        mat.use_nodes = True 
        nodes = mat.node_tree.nodes
        texture_node =nodes.new('ShaderNodeTexImage')
        texture_node.name = 'Bake_node'
        texture_node.select = True
        nodes.active = texture_node
        texture_node.image = img 

    bpy.ops.object.bake(type='COMBINED', pass_filter={'DIRECT','INDIRECT', 'EMIT', 'DIFFUSE', 'COLOR', 'GLOSSY', 'TRANSMISSION'}, filepath='', width=512, height=512, margin=16, use_selected_to_active=False, max_ray_distance=0.0, cage_extrusion=0.0, cage_object='', normal_space='TANGENT', normal_r='POS_X', normal_g='POS_Y', normal_b='POS_Z', target='IMAGE_TEXTURES', save_mode='INTERNAL', use_clear=False, use_cage=False, use_split_materials=False, use_automatic_name=False, uv_layer='')
    print("after bake")
    for mat in object.data.materials:
        for node in mat.node_tree.nodes:
            if node.name == 'Bake_node':
                mat.node_tree.nodes.remove(node)
    bpy.data.objects[object.name].select_set(False)
    