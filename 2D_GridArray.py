import bpy
import mathutils
from math import sqrt

sizeX = 192
sizeY = 108

aspectX = 16
aspectY = 9

numBuildings = 10000
collectionName = str(numBuildings)
cubeDimensionScale = 0.9
bpy.data.collections.new(collectionName)
resultCollection = bpy.data.collections[collectionName]
bpy.context.scene.collection.children.link(resultCollection)

rows = round((sqrt((((aspectX / aspectY) * numBuildings) + ((pow((aspectX - aspectY), 2)) / (pow((4 * aspectY), 2)))))) - ((aspectX - aspectY) / (2 * aspectY)))
columns = round(numBuildings / rows)
print(("Rows: %f, Columns: %f")%(rows,columns))

rowSize = sizeX / (rows-1)
columnSize = sizeY / (columns - 1) 
cubeDimensions = rowSize * cubeDimensionScale
bpy.ops.mesh.primitive_cube_add(location=(0,0,0))
tmpCube = bpy.context.active_object
tmpCube.dimensions = cubeDimensions,cubeDimensions,cubeDimensions
bpy.ops.object.transform_apply(location=False, rotation=False, scale=True)
for x in range(0,rows):
    xPos = rowSize * x
    print("row: %i"%x)
    
    for y in range(0, columns):
        yPos = columnSize * y
        new_object = bpy.data.objects.new(name=tmpCube.name, object_data=tmpCube.data)
        new_object.location = (xPos,yPos,0)
        resultCollection.objects.link(new_object)
  