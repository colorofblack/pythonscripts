import bpy

objectCounter = 1

if "Collection" in bpy.data.collections:
    sourceCollection = bpy.data.collections["Collection"]
if "Decimate" not in bpy.data.collections:
    bpy.data.collections.new("Decimate")
    decimatedCollection = bpy.data.collections["Decimate"]
    bpy.context.scene.collection.children.link(decimatedCollection)
else:
    decimatedCollection = bpy.data.collections["Decimate"]
numObjectsInCollection = len(sourceCollection.objects)
for obj in sourceCollection.objects:

    new_object = bpy.data.objects.new(name = obj.name, object_data = obj.data)
    new_object.modifiers.new("decimate", type='DECIMATE')
    new_object.modifiers["decimate"].ratio = 0.5
    new_object.data.use_auto_smooth = True
    new_object.modifiers.new("dataTransfer", type="DATA_TRANSFER")
    new_object.modifiers["dataTransfer"].object = obj
    new_object.modifiers["dataTransfer"].use_loop_data = True
    new_object.modifiers["dataTransfer"].data_types_loops = {'CUSTOM_NORMAL'}
    decimatedCollection.objects.link(new_object)
    print("Processing Mesh Number: ",objectCounter,"   out of: ",numObjectsInCollection)
    objectCounter = objectCounter + 1



    


    
