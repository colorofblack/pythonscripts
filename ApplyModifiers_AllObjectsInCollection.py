import bpy 
from bpy import context

for obj in bpy.data.collections["Decimate"].objects:
    #obj.data = obj.data.copy()
    context.view_layer.objects.active = obj
    targetObject = bpy.context.object

    for modifier in targetObject.modifiers:
        bpy.ops.object.modifier_apply(modifier=modifier.name)
