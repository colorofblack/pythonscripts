import plotly.graph_objects as go
import json

combinedValues = []
titles = []

with open('data.json') as f: 
    data = json.load(f)

for d in data['bauwerksWerte']:
    combinedValues.append(d['combinedValues']/4)
    if(d['Element'] != 'null'):
        element = d['Element']
        print(element)
        combinedString = d['Bauwerk'] +str(element)
        titles.append(combinedString)
print(combinedValuesTest)

fig = go.Figure(data=go.Bar(y=combinedValuesTest, x=titles))
fig.write_html('first_figure.html', auto_open=True)