import bpy

class MESH_OT_highriseBuilder(bpy.types.Operator):
    """Create Highrise"""
    bl_idname = "mesh.highrise_builder"
    bl_label = "Highrise Builder"
    bl_options = {'REGISTER', 'UNDO'}
    
    highriseID: bpy.props.IntProperty(
        name = "Highrise Type",
        description = "Type of Highrise to create",
        default = 1,
    )
    highriseLevels: bpy.props.IntProperty(
        name = "Highrise Levels",
        description = "Number of Levels on the Highrise",
        default = 1,
    )
    
    def execute(self, contex):
        count = 0
        zPos = 0
        while count <= self.highriseLevels:
            bpy.ops.mesh.primitive_monkey_add(
                size=1,
                location = (0, 0, zPos)
            )
            activeObject = bpy.context.active_object
            print(activeObject.data.dimensions)
           
            count = count +1
        #for obj in bpy.data.objects:
        #    print(obj.name)
        return {'FINISHED'}
    
def register():
    bpy.utils.register_class(MESH_OT_highriseBuilder)
    bpy.types.VIEW3D_MT_object.append(menu_func)
        
def unregister():
    bpy.utils.unregister_class(MESH_OT_highriseBuilder)
    bpy.types.VIEW3D_MT_object.append(menu_func)
    
def menu_func(self, context):
    self.layout.operator(MESH_OT_highriseBuilder.bl_idname)
    
if __name__ == '__main__':
    register()