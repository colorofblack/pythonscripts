import bpy 

for obj in bpy.data.collections["Collection"].objects:
    bpy.data.objects[obj.name].select_set(True)
    bpy.ops.object.transform_apply(location = False, scale = True, rotation = True)