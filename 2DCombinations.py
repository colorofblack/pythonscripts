list01 = ['A','B','C']
list02 = ['d','e','f']
list03 = ['x','y','z']
collection = []
for obj01 in list01:
    for obj02 in list02:
        for obj03 in list03:
            tmpArray = [obj01, obj02, obj03]
            collection.append(tmpArray)
print(collection)

twoDimArray = [list01, list02, list03]



def print1(arr):
     
    # number of arrays
    n = len(arr)
 
    # to keep track of next element 
    # in each of the n arrays
    indices = [0 for i in range(n)]
 
    while (1):
 
        # prcurrent combination
        for i in range(n):
            print(arr[i][indices[i]], end = " ")
        print()
 
        # find the rightmost array that has more
        # elements left after the current element
        # in that array
        next = n - 1
        while (next >= 0 and
              (indices[next] + 1 >= len(arr[next]))):
            next-=1
 
        # no such array is found so no more
        # combinations left
        if (next < 0):
            return
 
        # if found move to next element in that
        # array
        indices[next] += 1
 
        # for all arrays to the right of this
        # array current index again points to
        # first element
        for i in range(next + 1, n):
            indices[i] = 0

print1(twoDimArray)